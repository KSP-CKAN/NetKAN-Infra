This pull request was automatically generated by $site_name on behalf of $username, to add [$name]($mod_url) to CKAN.

Please direct questions about this pull request to [$username]($user_url).

## Mod details

Key         | Value
:---------- | :---------------------
Name        | [$name]($mod_url)
Author      | [$username]($user_url)
Abstract    | $short_description
License     | $license
Homepage    | <$external_link>
Source code | <$source_link>

## Description

$description
